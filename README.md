Language Switcher Enhanced
==========================

CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
This module overrides the language block to place your language switcher. 
It copies the default inner working but adds new options for structure, display
and options for when there are no translations.

REQUIREMENTS
------------
This module requires no modules outside of Drupal core.

INSTALLATION
------------
Install the "Language Switcher enhanced" module as
you would normally install a contributed Drupal module.

Visit https://www.drupal.org/node/1897420 for further
information.

CONFIGURATION
-------------
This module contains 4 configuration options:

## Display
Select how you want to display your language.
1. Using the language id, ie. nl / en / ....
2. Regular, translated language name ie English, Dutch, French, ...
3. Native language which uses the language name in their
native language ie English, Nederlands, Francais, ...

## Structure
Structure determines how we display the languages structure.
1. In a simple list
2. In a dropdown with the active language in the dropdown list
3. In a dropdown without the active language in the dropdown list.

## Hidden languages
We can hide languages from being displayed in the language switcher. Maybe you
haven't fully translated a certain language yet and want to soft-hide it for 
your users. A permission is used to enable it for different roles.

## No translation
Select what we want to do when a entity has no translation, disable the link, 
redirect to the homepage or do nothing.

DEVELOPERS
----------
* Kenny Derwael - https://www.drupal.org/u/zerdiox

MAINTAINERS
-----------
Current maintainers:
* Kenny Derwael - https://www.drupal.org/u/zerdiox
* Jeroen Tubex - https://www.drupal.org/u/jeroent
