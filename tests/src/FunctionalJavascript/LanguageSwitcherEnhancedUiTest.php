<?php

namespace Drupal\Tests\language_switcher_enhanced\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Tests the Language Switcher Enhanced user interface.
 *
 * @package Drupal\Tests\language_switcher_enhanced\FunctionalJavascript
 * @group language_switcher_enhanced
 */
class LanguageSwitcherEnhancedUiTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['block', 'language', 'language_switcher_enhanced'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  public function setUp(): void {
    parent::setUp();

    ConfigurableLanguage::create(['id' => 'es'])->save();
    ConfigurableLanguage::create(['id' => 'fr'])->save();
    $this->rebuildContainer();

    $settings = ['hidden_languages' => ['fr']];
    $this->drupalPlaceBlock('language_switcher_enhanced_block:language_interface', $settings);
  }

  /**
   * Tests language switch links generation.
   */
  public function testLanguageSwitchLinks() {
    $session = $this->getSession();
    $page = $session->getPage();

    $this->drupalGet('<front>');
    $this->assertSession()->buttonExists('English');
    $page->pressButton('English');
  }

}
