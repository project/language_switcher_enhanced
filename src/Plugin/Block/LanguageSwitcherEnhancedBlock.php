<?php

namespace Drupal\language_switcher_enhanced\PLugin\Block;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\language\Plugin\Block\LanguageBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Enhanced language switcher block.
 *
 * @Block(
 *   id = "language_switcher_enhanced_block",
 *   admin_label = @Translation("Enhanced language switcher"),
 *   category = @Translation("System"),
 *   deriver = "Drupal\language\Plugin\Derivative\LanguageBlock"
 * )
 */
class LanguageSwitcherEnhancedBlock extends LanguageBlock {

  protected const CONFIG_DISPLAY_TRANSLATED = 'translated';

  protected const CONFIG_DISPLAY_ID = 'id';

  protected const CONFIG_DISPLAY_NATIVE = 'native';

  protected const CONFIG_STRUCTURE_LIST = 'list';

  protected const CONFIG_STRUCTURE_NESTED = 'nested';

  protected const CONFIG_STRUCTURE_NESTED_ACTIVE = 'nested_active';

  protected const CONFIG_NOT_TRANSLATED_DEFAULT = 'default';

  protected const CONFIG_NOT_TRANSLATED_HOMEPAGE = 'homepage';

  protected const CONFIG_NOT_TRANSLATED_DISABLED = 'disabled';

  /**
   * Routematcher provided by Drupal.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * Account from user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, LanguageManagerInterface $language_manager, PathMatcherInterface $path_matcher, CurrentRouteMatch $route_match, AccountProxyInterface $user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $language_manager, $path_matcher);
    $this->routeMatch = $route_match;
    $this->user = $user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('path.matcher'),
      $container->get('current_route_match'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['display'] = $form_state->getValue('display');
    $this->configuration['structure'] = $form_state->getValue('structure');
    $this->configuration['not_translated'] = $form_state->getValue('not_translated');
    $this->configuration['hidden_languages'] = $form_state->getValue('hidden_languages');
    $this->configuration['use_bootstrap'] = $form_state->getValue('use_bootstrap');
  }

  /**
   * The block form used in back-end.
   *
   * @inheritdoc
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->configuration;
    $form['display'] = [
      '#type' => 'select',
      '#title' => $this->t('Language display'),
      '#description' => $this->t('Determines the display of a language, either as an ID or fully written out. Be sure to translate the language in config if you want translated names.'),
      '#default_value' => $config['display'],
      '#required' => TRUE,
      '#options' => $this->getDisplayOptions(),
    ];

    $form['structure'] = [
      '#type' => 'select',
      '#title' => $this->t('Language structure'),
      '#description' => $this->t('Determines how the language options as structured in HTML. Dropdown or in a list.'),
      '#default_value' => $config['structure'],
      '#required' => TRUE,
      '#options' => $this->getStructureOptions(),
    ];

    $form['not_translated'] = [
      '#type' => 'select',
      '#title' => $this->t('Redirect to homepage'),
      '#description' => $this->t('If the link to the translated content ends on a non-translated entity link to homepage.'),
      '#default_value' => $config['not_translated'],
      '#required' => TRUE,
      '#options' => $this->getNotTranslatedOptions(),
    ];

    $form['hidden_languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Hide language switch'),
      '#description' => $this->t('Hide the language for people who do not have the right permissions. <strong>Will not redirect unauthorized users.</strong>'),
      '#default_value' => $config['hidden_languages'],
      '#options' => $this->getHiddenLanguagesOptions(),
    ];

    $form['use_bootstrap'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use bootstrap drop down.'),
      '#description' => $this->t('This will load in a part of the bootstrap library to facilitate drop-down behaviour and mark-up. Leave this disabled when using your own styling, or from the theme.'),
      '#default_value' => $config['use_bootstrap'],
    ];

    return $form;
  }

  /**
   * Returns display options.
   *
   * @return array
   *   Returned options
   */
  protected function getDisplayOptions(): array {
    return [
      self::CONFIG_DISPLAY_TRANSLATED => $this->t('Translated language name'),
      self::CONFIG_DISPLAY_ID => $this->t('Only language ID'),
      self::CONFIG_DISPLAY_NATIVE => $this->t('Language name in their own language'),
    ];
  }

  /**
   * Returns structured options.
   *
   * @return array
   *   Returned options
   */
  protected function getStructureOptions(): array {
    return [
      self::CONFIG_STRUCTURE_LIST => $this->t('Languages in a single list'),
      self::CONFIG_STRUCTURE_NESTED => $this->t('Dropdown with active language as top and NOT repeated in list'),
      self::CONFIG_STRUCTURE_NESTED_ACTIVE => $this->t('Dropdown with active language as top and repeated in list'),
    ];
  }

  /**
   * Returns structure kets for use in module.
   *
   * @return array
   *   Returned options.
   */
  public static function getStructureKeys(): array {
    return [
      self::CONFIG_STRUCTURE_LIST,
      self::CONFIG_STRUCTURE_NESTED,
      self::CONFIG_STRUCTURE_NESTED_ACTIVE,
    ];
  }

  /**
   * Returns translated options.
   *
   * @return array
   *   Returned options
   */
  protected function getNotTranslatedOptions(): array {
    return [
      self::CONFIG_NOT_TRANSLATED_DEFAULT => 'Follow Drupal default',
      self::CONFIG_NOT_TRANSLATED_HOMEPAGE => 'Redirect to homepage',
      self::CONFIG_NOT_TRANSLATED_DISABLED => 'Disable link',
    ];
  }

  /**
   * Returns hidden language options.
   *
   * @return array
   *   Returned options
   */
  protected function getHiddenLanguagesOptions(): array {
    $languages = [];
    foreach ($this->languageManager->getLanguages() as $language) {
      $languages[$language->getId()] = $language->getName();
    }
    return $languages;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'display' => self::CONFIG_DISPLAY_TRANSLATED,
      'structure' => self::CONFIG_STRUCTURE_NESTED_ACTIVE,
      'not_translated' => FALSE,
      'hidden_languages' => [],
      'use_bootstrap' => 1,
    ];
  }

  /**
   * Builds array used for rendering.
   *
   * @return array
   *   Build array
   */
  public function build(): array {
    $type = $this->getDerivativeId();
    $current_lang_id = $this->languageManager->getCurrentLanguage($type)->getId();

    $links = $this->getLinks();

    if (!$links) {
      return [];
    }

    $this->changeLinksAreTranslated($links);

    $this->filterHiddenLanguages($links);

    // If you hide everything we don't have links.
    if (empty($links)) {
      return [];
    }

    $active_language = $links[$current_lang_id] ?? NULL;
    $structure = $this->structureLinks($links);

    $build = [
      '#theme' => 'language_switcher__' . $structure,
      '#active_language' => $active_language,
      '#languages' => $links,
    ];

    $config = $this->getConfiguration();
    $bootstrap = $config['use_bootstrap'];
    if ($bootstrap) {
      $build['#attached']['library'][] = 'language_switcher_enhanced/bootstrap';
    }
    return $build;
  }

  /**
   * Gets all links to different languages.
   *
   * @return array|bool
   *   If we have links you get links.
   */
  protected function getLinks() {
    $url = $this->pathMatcher->isFrontPage() ? Url::fromRoute('<front>') : Url::fromRouteMatch($this->routeMatch);
    $type = $this->getDerivativeId();
    /** @var \stdClass $links */
    $links = $this->languageManager->getLanguageSwitchLinks($type, $url);

    if (empty($links) || !property_exists($links, 'links')) {
      return FALSE;
    }

    $language_links = [];
    foreach ($links->links as $link) {

      if (!$link['language'] instanceof ConfigurableLanguage) {
        continue;
      }

      if (!$link['url'] instanceof Url) {
        continue;
      }
      // Build our links to other languages.
      $language = $link['language'];
      $url = $link['url'];

      $url->setOption('language', $language);
      $language_id = $language->getId();
      $language_links[$language_id] = $link;
      $language_links[$language_id]['title'] = $this->getLanguageTitle($language);
      unset($language_links[$language_id]['attributes'], $language_links[$language_id]['query']);
    }

    return $language_links;
  }

  /**
   * Gets link titles based on config getDisplayOptions.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The Drupal language.
   *
   * @return string
   *   Returned title for language
   */
  protected function getLanguageTitle(LanguageInterface $language): string {
    $config = $this->getConfiguration();
    $display = $config['display'];
    if ($display === self::CONFIG_DISPLAY_ID) {
      return $language->getId();
    }

    if ($display === self::CONFIG_DISPLAY_TRANSLATED) {
      $languages = $this->languageManager->getLanguages();
      return $languages[$language->getId()]->getName();
    }

    if ($display === self::CONFIG_DISPLAY_NATIVE) {
      $native_languages = $this->languageManager->getNativeLanguages();
      return $native_languages[$language->getId()]->getName();
    }

    return $language->getName();
  }

  /**
   * Change links based on settings.
   *
   * We check if the target of the URL to the different language has a
   * translation and if we want to redirect to homepage if it doesn't have one.
   *
   * @param array $links
   *   The list of language links used in the language switcher.
   */
  protected function changeLinksAreTranslated(array &$links): void {
    $config = $this->getConfiguration();
    $not_translated = $config['not_translated'];

    if ($not_translated === self::CONFIG_NOT_TRANSLATED_DEFAULT) {
      return;
    }

    $route_name = $this->routeMatch->getRouteName();
    $route_name_parts = explode('.', $route_name ?? '');
    // First route part should be an entity. And third canonical so we know we
    // are on a display page. Otherwise we cannot check translations.
    // The second part should then be the entity type we can retrieve from the
    // parameters.
    if ($route_name_parts[0] !== 'entity' || !isset($route_name_parts[2]) || $route_name_parts[2] !== 'canonical') {
      return;
    }
    $entity = $this->routeMatch->getParameter($route_name_parts[1]);
    if (!$entity instanceof ContentEntityInterface) {
      return;
    }
    foreach ($links as $language_id => $link) {
      $has_translation = $entity->hasTranslation($language_id);
      if ($has_translation !== TRUE) {

        if ($not_translated === self::CONFIG_NOT_TRANSLATED_HOMEPAGE) {
          $language = $this->languageManager->getLanguage($language_id);
          $links[$language_id]['url'] = new Url('<front>', [], ['language' => $language]);
        }
        elseif ($not_translated === self::CONFIG_NOT_TRANSLATED_DISABLED) {
          $links[$language_id]['url'] = new Url('<nolink>');
        }

      }
    }
  }

  /**
   * Filters hidden languages from the list of links.
   *
   * @param array $links
   *   The list of language links used in the language switcher.
   */
  protected function filterHiddenLanguages(array &$links): void {
    $config = $this->getConfiguration();
    if ($this->user->hasPermission('see hidden languages switcher')) {
      return;
    }
    $hidden_languages = $config['hidden_languages'];
    foreach ($hidden_languages as $hidden_language) {
      if (isset($links[$hidden_language])) {
        unset($links[$hidden_language]);
      }
    }
  }

  /**
   * Structures our links for how we want them displayed.
   *
   * @param array $links
   *   An array containing menu links.
   *
   * @return string
   *   The used structure.
   */
  protected function structureLinks(array &$links): string {
    $config = $this->getConfiguration();
    $structure = $config['structure'];

    if ($structure === self::CONFIG_STRUCTURE_NESTED) {
      $current_language_id = $this->languageManager->getCurrentLanguage()
        ->getId();
      unset($links[$current_language_id]);
    }

    return $structure;
  }

}
